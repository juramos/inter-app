# inter-app

## Como configurar o banco de dados

Crie um banco MySQL. Exponha o banco na porta 3306. Entre no banco e execute os seguintes comandos.

```sql
CREATE DATABASE inter_app;
USE inter_app;
CREATE TABLE Users (
	email varchar(60),
	name varchar(60) NOT NULL,
	publicKey BLOB,
	PRIMARY KEY(email)
);
CREATE TABLE UniqueDigits (
	id int NOT NULL AUTO_INCREMENT,
	n varchar(255) NOT NULL,
	k integer NOT NULL,
	result integer NOT NULL,
	user varchar(60),
	PRIMARY KEY(id),
	FOREIGN KEY (user) REFERENCES Users(email)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);
CREATE USER 'inter_user'@'localhost' IDENTIFIED BY 'pass';
GRANT INSERT, DELETE, SELECT, UPDATE ON inter_app.* TO 'inter_user'@'localhost';
FLUSH PRIVILEGES;
```

## Como executar a aplicação

Na raiz do projeto execute

```bash
mvn jetty:run
```

## Como executar os testes unitários

Na raiz do projeto execute

```bash
mvn test
```

## Testes integrados com Postman

Os testes dentro de cada pasta devem ser executados em ordem crescente de acordo com o número no nome do teste.

## Como gerar chaves públicas

Na raiz do projeto execute

```bash
java RSAGenerateKeys.java
```

## Parâmetro n do dígito único

Na especificação do desafio o parâmetro n do dígito único é descrito como 1 <= n <= 10^1000000. Mas eu não sabia como armazenar esse número no banco e como testar no código se ele não era maior do que deveria. Por isso minha aplicação suporta os seguintes valores de n: 1 <= n <= (10^255)-1.