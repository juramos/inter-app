package com.inter.rsa.helpers;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class RSAGenerateKeys {
		
	public static void main(String[] args) {		
		try {
			KeyPairGenerator generator;
			generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(2048);
			KeyPair pair = generator.generateKeyPair();
			
			PublicKey publicKey = pair.getPublic();
			PrivateKey privateKey = pair.getPrivate();
			
			byte[] publicKeyBytes = publicKey.getEncoded();
			byte[] privateKeyBytes = privateKey.getEncoded();
			
			String publicKeyString = Base64.getUrlEncoder().encodeToString(publicKeyBytes);
			String privateKeyString = Base64.getUrlEncoder().encodeToString(privateKeyBytes);
						
			System.out.println("Public key: ");
			System.out.println(publicKeyString);			
			System.out.println("Private key: ");
			System.out.println(privateKeyString);
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
}
