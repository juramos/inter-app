package com.inter.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.inter.user.User;

@SuppressWarnings("serial")
public class UserServlet extends HttpServlet {
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String email = request.getParameter("email");
		String name = request.getParameter("name");
		String publicKey = request.getParameter("publicKey");
		
		JSONObject json = new JSONObject();
		
		if (email == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		} 
		
		if (name == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("name", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		} 
		
		if (email.length() > User.MAX_EMAIL_LEN) {
			json.put("status", "fail");
			json.put("data", Response.createObject(
				"email", 
				"cannot be longer than " + User.MAX_EMAIL_LEN + " characters."
			));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (name.length() > User.MAX_NAME_LEN) {
			json.put("status", "fail");
			json.put("data", Response.createObject(
				"name", 
				"cannot be longer than " + User.MAX_NAME_LEN + " characters."
			));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (publicKey != null && publicKey.length() > User.MAX_KEY_LEN) {
			json.put("status", "fail");
			json.put("data", Response.createObject(
				"publicKey", 
				"cannot be longer than " +  User.MAX_KEY_LEN + " characters."
			));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		User user = User.createUser(email, name, publicKey);
		if (user == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "already in use."));
			
		} else {
			json.put("status", "success");
			json.put("data", Response.createObject(user));
		}
		
		response.getOutputStream().println(json.toString());
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
		String email = request.getParameter("email");
		
		JSONObject json = new JSONObject();
		
		if (email == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		} 
		
		User user = User.readUser(email);
		if(user == null){
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "user does not exist."));
		} else {
			json.put("status", "success");
			json.put("data", Response.createObject(user));
		}
		
		response.getOutputStream().println(json.toString());
	}
	
	@Override
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String oldEmail = request.getParameter("email");
		String newEmail = request.getParameter("newEmail");
		String newName = request.getParameter("newName");
		String newPublicKey = request.getParameter("newPublicKey");
		
		JSONObject json = new JSONObject();
		
		if (oldEmail == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (newEmail != null && newEmail.length() > User.MAX_EMAIL_LEN) {
			json.put("status", "fail");
			json.put("data", Response.createObject(
				"newEmail", 
				"cannot be longer than " + User.MAX_EMAIL_LEN + " characters."
			));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (newName != null && newName.length() > User.MAX_NAME_LEN) {
			json.put("status", "fail");
			json.put("data", Response.createObject(
				"newName", 
				"cannot be longer than " + User.MAX_NAME_LEN + " characters."
			));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (newPublicKey != null && newPublicKey.length() > User.MAX_KEY_LEN) {
			json.put("status", "fail");
			json.put("data", Response.createObject(
				"newPublicKey", 
				"cannot be longer than " +  User.MAX_KEY_LEN + " characters."
			));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		User user = User.readUser(oldEmail);
		if (user == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "user does not exist."));
			response.getOutputStream().println(json.toString());
			return;
		} 
		
		if (user.update(newEmail, newName, newPublicKey)) {
			json.put("status", "success");
			json.put("data", Response.createObject(user));
		} else {
			json.put("status", "fail");
			json.put("data", Response.createObject("newEmail", "already in use."));
		}
			
		response.getOutputStream().println(json.toString());
	}
	
	@Override
	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {	
		String email = request.getParameter("email");
		
		JSONObject json = new JSONObject();	
		
		if (email == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (User.deleteUser(email)) {
			json.put("status", "success");
			json.put("data", new JSONObject());
		} else {
			json.put("status", "fail");
			json.put("data", Response.createObject("email", "user does not exist."));
		}
		
		response.getOutputStream().println(json.toString());
	}
	
}
