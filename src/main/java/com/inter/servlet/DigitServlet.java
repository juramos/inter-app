package com.inter.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.inter.digit.UniqueDigit;
import com.inter.user.User;

@SuppressWarnings("serial")
public class DigitServlet extends HttpServlet {
		
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String n = request.getParameter("n");
		String k = request.getParameter("k");
		String email = request.getParameter("email");
		
		JSONObject json = new JSONObject();
		if (n == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("n", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (k == null) {
			json.put("status", "fail");
			json.put("data", Response.createObject("k", "cannot be null."));
			response.getOutputStream().println(json.toString());
			return;
		} 
		
		if (email != null) {
			User user = User.readUser(email);
			if (user == null) {
				json.put("status", "fail");
				json.put("data", Response.createObject("email", "user does not exist."));
				response.getOutputStream().println(json.toString());
				return;
			}
		}
		
		int numberK = 0;
		try {
			numberK = Integer.parseInt(k);
		} catch (NumberFormatException e) {
			json.put("status", "fail");
			json.put("data", 
				Response.createObject("k", "invalid.")
			);
			response.getOutputStream().println(json.toString());
			return;
		}
				
		if (numberK < UniqueDigit.MIN_K) {
			json.put("status", "fail");
			json.put("data", 
				Response.createObject("k", "cannot be less than " + UniqueDigit.MIN_K + ".")
			);
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (numberK > UniqueDigit.MAX_K) {
			json.put("status", "fail");
			json.put("data", 
				Response.createObject("k", "cannot be greater than " + UniqueDigit.MAX_K + ".")
			);
			response.getOutputStream().println(json.toString());
			return;
		}
		
		if (n.length() > UniqueDigit.MAX_N_LEN) {
			json.put("status", "fail");
			json.put("data", 
				Response.createObject("n", "cannot be greater than (10^" + UniqueDigit.MAX_N_LEN + ")-1.")
			);
			response.getOutputStream().println(json.toString());
			return;
		}
		
		UniqueDigit digit = UniqueDigit.createUniqueDigit(n, numberK, email);
		json.put("status", "success");
		json.put("data", Response.createObject(digit));
		response.getOutputStream().println(json.toString());
	}
}
