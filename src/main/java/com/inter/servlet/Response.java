package com.inter.servlet;

import org.json.JSONObject;

import com.inter.digit.UniqueDigit;
import com.inter.rsa.RSA;
import com.inter.user.User;

public class Response {
	
	public static JSONObject createObject(String field, String message) {
		JSONObject json = new JSONObject();
		json.put(field, message);
		return json;
	}
	
	public static JSONObject createObject(User user) {
		JSONObject json = new JSONObject();
		json.put("email", RSA.encrypt(user.getPublicKey(), user.getEmail()));
		json.put("name", RSA.encrypt(user.getPublicKey(), user.getName()));
		json.put("list", user.getList());
		
		JSONObject json2 = new JSONObject();
		json2.put("user", json);
		return json;
	}
	
	public static JSONObject createObject(UniqueDigit digit) {
		JSONObject json = new JSONObject();
		json.put("result", digit.getResult());
		return json;
	}
	
}
