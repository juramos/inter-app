package com.inter.digit;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

class DigitData {
	
	private final String DB_URL = "jdbc:mysql://localhost:3306/inter_app?verifyServerCertificate=false&useSSL=true";
	private final String USER = "inter_user";
	private final String PASS = "pass";
	private Connection conn; 
	
	DigitData() {
		// Open a connection
		try{
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(true);
		} catch (SQLException e) {
           e.printStackTrace();
        }
	}
	
	void record(UniqueDigit digit, String email) {
		try {
			PreparedStatement statement = conn.prepareStatement("INSERT INTO UniqueDigits VALUES (NULL, ?, ?, ?, ?);");
			statement.setString(1, digit.getN());
			statement.setInt(2, digit.getK());
			statement.setInt(3, digit.getResult());
			statement.setString(4, email);
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	List<UniqueDigit> readAllOfUser(String email) {
		try {
			PreparedStatement statement = conn.prepareStatement("SELECT n, k, result FROM UniqueDigits WHERE user = ?;");
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
			
			List<UniqueDigit> list = new ArrayList<UniqueDigit>();
			
			while (rs.next()) {
				list.add(new UniqueDigit(
						rs.getString("n"), 
						rs.getInt("k"),
						rs.getInt("result")
				));
	        }
			rs.close();
			statement.close();
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}