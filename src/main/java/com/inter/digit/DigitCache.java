package com.inter.digit;

import org.javatuples.Pair;

import com.inter.cache.Cache;

class DigitCache extends Cache<Pair<String, Integer>, Integer>{
	
	public DigitCache(int size) {
		super(size);
	}

	@Override
	protected Integer calculate(Pair<String, Integer> key) {
		return UniqueDigit.calculate(key.getValue0(), key.getValue1());		
	}
}
