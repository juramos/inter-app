package com.inter.digit;

import java.util.List;

import org.javatuples.Pair;

public class UniqueDigit {
	
	private String n;
	private int k;
	private int result;
	
	private static DigitCache cache = new DigitCache(10);
	private static DigitData data = new DigitData();	
	
	public static final int MAX_K = 100000;
	public static final int MIN_K = 1;
	public static final int MAX_N_LEN = 255;
	
	UniqueDigit(String n, int k, int result) {
		this.n = n;
    	this.k = k;
    	this.result = result;
	}
	
	public static UniqueDigit createUniqueDigit(String n, int k, String email) {
		UniqueDigit digit = new UniqueDigit(n, k, cache.get(new Pair<String, Integer>(n, k)));
    	data.record(digit, email);
    	return digit;
	}
	
	static public List<UniqueDigit> readAllOfUser(String email) {
		return data.readAllOfUser(email);
	}
	
	static int calculate(String n, int k) {
		int d, sum = 0;
		for (char c: n.toCharArray()) {
            d = Character.getNumericValue(c);
            sum += d;
        }
		Integer result = sum * k;
		if (result.toString().length() > 1) {
			result = calculate(result.toString(), 1);
		}
		return result;
	}
	
	public String getN() {
		return n;
	}

	public int getK() {
		return k;
	}

	public int getResult() {
		return result;
	}
	
}