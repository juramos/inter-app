package com.inter.rsa;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSA {
	
	static private KeyFactory keyFactory;
	static private Cipher cipher;
	
	static {
		try {
			cipher = Cipher.getInstance("RSA");
			keyFactory = KeyFactory.getInstance("RSA");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}
	
	static public String encrypt(byte[] publicKeyBytes, String message) {
		if (publicKeyBytes == null) {
			return message;
		}
		try {
			PublicKey publicKey = keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] messageBytes = message.getBytes(StandardCharsets.UTF_8);
			byte[] encryptedMessageBytes;
			encryptedMessageBytes = cipher.doFinal(messageBytes);
			String encryptedMessageStr = Base64.getUrlEncoder().encodeToString(encryptedMessageBytes);
			return encryptedMessageStr;
		} catch (IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException | InvalidKeyException e) {
			e.printStackTrace();
		}
		return null;
	}
}
