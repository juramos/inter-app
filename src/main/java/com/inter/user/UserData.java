package com.inter.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.inter.digit.UniqueDigit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

class UserData {
	
	private final String DB_URL = "jdbc:mysql://localhost:3306/inter_app?verifyServerCertificate=false&useSSL=true";
	private final String USER = "inter_user";
	private final String PASS = "pass";
	private Connection conn; 
		
	UserData() {
		// Open a connection
		try{
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			conn.setAutoCommit(true);
		} catch (SQLException e) {
           e.printStackTrace();
        }
	}
		
	void record(User user) {
		try {
			PreparedStatement statement = conn.prepareStatement("INSERT INTO Users VALUES (?, ?, ?);");
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getName());
			statement.setBytes(3, user.getPublicKey());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	User read(String email) {
		User user = null;
		try {
			PreparedStatement statement = conn.prepareStatement("SELECT email, name, publicKey FROM Users WHERE email = ?;");
			statement.setString(1, email);
			ResultSet rs = statement.executeQuery();
	        if (rs.next()) {
	        	user = new User(rs.getString("email"), rs.getString("name"), rs.getBytes("publicKey"), new ArrayList<UniqueDigit>());
	        }
	        rs.close();
	        statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	void update(String oldEmail, User user) {
		try {
			PreparedStatement statement = conn.prepareStatement("UPDATE Users SET email = ?, name = ?, publicKey = ? WHERE email = ?;");
			statement.setString(1, user.getEmail());
			statement.setString(2, user.getName());
			statement.setBytes(3, user.getPublicKey());
			statement.setString(4, oldEmail);
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	void delete(User user) {
		try {
			PreparedStatement statement = conn.prepareStatement("DELETE FROM Users WHERE email = ?;");
			statement.setString(1, user.getEmail());
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}