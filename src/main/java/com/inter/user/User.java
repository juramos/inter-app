package com.inter.user;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.inter.digit.UniqueDigit;

public class User {
	
	private String email;
	private String name;
	byte[] publicKey;
	private List<UniqueDigit> list;
	private static UserData data = new UserData();
	
	public static final int MAX_EMAIL_LEN = 60;
	public static final int MAX_NAME_LEN = 60;
	public static final int MAX_KEY_LEN = 400;
		
	User(String email, String name, byte[] publicKey, List<UniqueDigit> list) {
		this.email = email;
		this.name = name;
		this.publicKey = publicKey;
		this.list = list;
	}
	
	public static User createUser(String email, String name, String publicKeyStr) {
		User user = readUser(email);
		if (user == null) {
			byte[] publicKeyBytes = null;
			if (publicKeyStr != null) {				
				publicKeyBytes = Base64.getUrlDecoder().decode(publicKeyStr);
			}
			user = new User(email, name, publicKeyBytes, new ArrayList<UniqueDigit>());			
			data.record(user);
			return user;
		} else {
			return null;
		}				
	}
	
	public static User readUser(String email) {
		User user = data.read(email);
		if (user != null) {
			user.list = UniqueDigit.readAllOfUser(email);
		}
		return user;
	}
	
	public boolean update(String newEmail, String newName, String newPublicKey) {
		String oldEmail = this.email;
		if (newEmail != null) {
			User test = User.readUser(newEmail);
			if (test != null) {
				return false;
			}
			this.email = newEmail;
		}
		if (newName != null) {
			this.name = newName;
		}
		if (newPublicKey != null) {
			this.publicKey = Base64.getUrlDecoder().decode(newPublicKey);
		}
		data.update(oldEmail, this);	
		return true;
	}
	
	public static boolean deleteUser(String email) {
		User user = readUser(email);
		if (user != null) {
			data.delete(user);
			return true;
		}
		
		return false;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getName() {
		return name;
	}

	public List<UniqueDigit> getList() {
		return list;
	}

	public byte[] getPublicKey() {
		return publicKey;
	}
	
}
