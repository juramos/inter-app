package com.inter.cache;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.javatuples.Pair;

public abstract class Cache<KeyType, ValueType> {
	private Map<KeyType, Pair<ValueType, LocalTime>> map = new HashMap<KeyType, Pair<ValueType, LocalTime>>(10, 1);
	private final int SIZE;
	
	public Cache(int size) {
		this.SIZE = size;
	}
	
	private void put(KeyType key, Pair<ValueType, LocalTime> value) {
		if (map.size() == SIZE) {
			Iterator<Entry<KeyType, Pair<ValueType, LocalTime>>> it = map.entrySet().iterator();			
			KeyType oldestKey = null;
			LocalTime oldestTime = LocalTime.now();
		    while (it.hasNext()) {
		        Map.Entry<KeyType, Pair<ValueType, LocalTime>> entry = it.next();
		        if (entry.getValue().getValue1().isBefore(oldestTime)) {
		        	oldestTime = entry.getValue().getValue1();
		        	oldestKey = entry.getKey();
		        }
		    }
		    map.remove(oldestKey);
		}
		map.put(key, value);
	}
	
	public ValueType get(KeyType key) {
		Pair<ValueType, LocalTime> value = map.get(key);
		if (value == null) {
			value = new Pair<ValueType, LocalTime>(calculate(key), LocalTime.now());
			put(key, value);
		} else {			
			map.replace(key, value.setAt1(LocalTime.now()));
		}
		return value.getValue0();
	}
	
	abstract protected ValueType calculate(KeyType key);
	
}

