package com.inter.rsa;

import org.junit.Assert;
import org.junit.Test;

public class TestRSA_encrypt_keyIsNull {

	@Test
	public void test() {
		String msg = "Test message: RSA_encrypt_keyIsNull.";		
		String encryptedMsgStr= RSA.encrypt(null, msg);
		
		Assert.assertEquals(encryptedMsgStr, msg);
	}

}
