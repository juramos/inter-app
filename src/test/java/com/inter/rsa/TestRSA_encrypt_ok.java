package com.inter.rsa;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Assert;
import org.junit.Test;

public class TestRSA_encrypt_ok {

	@Test
	public void test() {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(2048);
			KeyPair pair = generator.generateKeyPair();
			
			PublicKey publicKey = pair.getPublic();
			PrivateKey privateKey = pair.getPrivate();
			
			byte[] publicKeyBytes = publicKey.getEncoded();
			
			String msg = "Test message: RSA_encrypt_ok.";		
			String encryptedMsgStr= RSA.encrypt(publicKeyBytes, msg);		
			byte[] encryptedMsgBytes = Base64.getUrlDecoder().decode(encryptedMsgStr);
			
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] decryptedMsgBytes;
			decryptedMsgBytes = cipher.doFinal(encryptedMsgBytes);
			
			String decryptedMsg = new String(decryptedMsgBytes, StandardCharsets.UTF_8);		
			Assert.assertEquals(decryptedMsg, msg);		
		} catch (
			NoSuchAlgorithmException | 
			NoSuchPaddingException | 
			InvalidKeyException |
			IllegalBlockSizeException |
			BadPaddingException e
		) {
			e.printStackTrace();
		}
	}

}
