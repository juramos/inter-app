package com.inter.digit;

import org.junit.Assert;
import org.junit.Test;

public class TestUniqueDigit_createUniqueDigit_maxValues {

	@Test
	public void test() {
		String n = "999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999";
		int k = 100000;
		UniqueDigit digit = UniqueDigit.createUniqueDigit(n, k, null);
		Assert.assertEquals(n.toString(), digit.getN());
		Assert.assertEquals(k, digit.getK());
		Assert.assertEquals(9, digit.getResult());
	}

}
