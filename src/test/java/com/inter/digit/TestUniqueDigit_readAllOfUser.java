package com.inter.digit;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.inter.user.User;

public class TestUniqueDigit_readAllOfUser {

	@Before
	public void setUp() throws Exception {
		String email = "UniqueDigit_readAllOfUser@teste.com";
		User.createUser(email, "UniqueDigit_readAllOfUser", null);
		UniqueDigit.createUniqueDigit("1789", 1, email);
		UniqueDigit.createUniqueDigit("23", 5, email);
		UniqueDigit.createUniqueDigit("118", 2, email);
		UniqueDigit.createUniqueDigit("118", 2, email);
	}

	@Test
	public void test() {
		
		List<UniqueDigit> list = UniqueDigit.readAllOfUser("UniqueDigit_readAllOfUser@teste.com");
		Assert.assertEquals(4, list.size());
		
		UniqueDigit digit = list.get(0);
		Assert.assertEquals("1789", digit.getN());
		Assert.assertEquals(1, digit.getK());
		Assert.assertEquals(7, digit.getResult());
		
		digit = list.get(1);
		Assert.assertEquals("23", digit.getN());
		Assert.assertEquals(5, digit.getK());
		Assert.assertEquals(7, digit.getResult());
		
		digit = list.get(2);
		Assert.assertEquals("118", digit.getN());
		Assert.assertEquals(2, digit.getK());
		Assert.assertEquals(2, digit.getResult());
		
		digit = list.get(3);
		Assert.assertEquals("118", digit.getN());
		Assert.assertEquals(2, digit.getK());
		Assert.assertEquals(2, digit.getResult());
	}

	@After
    public void clean() {
    	User.deleteUser("UniqueDigit_readAllOfUser@teste.com");
    }
}
