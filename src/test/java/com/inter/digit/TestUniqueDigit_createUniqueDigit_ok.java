package com.inter.digit;

import org.junit.Assert;
import org.junit.Test;

public class TestUniqueDigit_createUniqueDigit_ok {

	@Test
	public void test() {
		UniqueDigit digit = UniqueDigit.createUniqueDigit("9875", 4, null);
		Assert.assertEquals("9875", digit.getN());
		Assert.assertEquals(4, digit.getK());
		Assert.assertEquals(8, digit.getResult());
	}

}
