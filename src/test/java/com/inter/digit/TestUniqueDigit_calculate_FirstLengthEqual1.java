package com.inter.digit;

import org.junit.Assert;
import org.junit.Test;

public class TestUniqueDigit_calculate_FirstLengthEqual1 {

	@Test
	public void test() {
		UniqueDigit digit = UniqueDigit.createUniqueDigit("1233", 1, null);
		Assert.assertEquals("1233", digit.getN());
		Assert.assertEquals(1, digit.getK());
		Assert.assertEquals(9, digit.getResult());
	}

}
