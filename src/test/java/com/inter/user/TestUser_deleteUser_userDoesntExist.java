package com.inter.user;

import org.junit.Assert;
import org.junit.Test;

public class TestUser_deleteUser_userDoesntExist {

	@Test
	public void test() {
		Assert.assertEquals(false, User.deleteUser("User_deleteUser_userDoesntExist@teste.com"));
	}

}
