package com.inter.user;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import com.inter.digit.UniqueDigit;

public class TestUser_createUser_publicKeyIsNull {


	@Test
	public void test() {
		String email = "User_createUser_publicKeyIsNull@teste.com";
		String name = "User_createUser_publicKeyIsNull";
		User user =  User.createUser(email, name, null);
		
		Assert.assertEquals(user.getEmail(), email);
		Assert.assertEquals(user.getName(), name);
		Assert.assertEquals(user.getPublicKey(), null);
		Assert.assertEquals(user.getList(), new ArrayList<UniqueDigit>());
	}
    
    @After
    public void clean() {
    	User.deleteUser("User_createUser_publicKeyIsNull@teste.com");
    }

}
