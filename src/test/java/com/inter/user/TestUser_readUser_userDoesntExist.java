package com.inter.user;

import org.junit.Assert;
import org.junit.Test;

public class TestUser_readUser_userDoesntExist {

	@Test
	public void test() {
		Assert.assertEquals(null, User.readUser("User_readUser_userDoesntExist@teste.com"));
	}

}
