package com.inter.user;

import java.util.ArrayList;
import java.util.Base64;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.inter.digit.UniqueDigit;

public class TestUser_update_newKeyIsNull {

	@Before
    public void setUp() {
		User.createUser(
        		"User_update_newKeyIsNull@teste.com", 
        		"User_update_newKeyIsNull",
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhJexIekccp89SoWOn092roUid_-oYE33VWgYbmdMZMIVubwSp9GnU6woPYew22hlUEw5sGpQjvwYelp6-tziOzLLnz96v3HMp9fK1oGhsmxzMecmiVQ52XksbSztCLVhm5zFB0PQbKgvPtKJwDOywmGgfFE5UY5fldtI0BSJXYb7K5t6R5bMaRblwoDyz4U3S4-M75HkA4Ocz_zyznsvtVFh0jqqwu2Kc3DzO05y_MNYyG_XOii8RTCoJrZL1blfmvfwo_S1AvEZBlNXlw4RpcsVRK8Ci8RvgFJHAAnjTHK3tRiV_JkwVqlZASIgVx7IkJUL4KpKQ1HNu71aTB55oQIDAQAB"
        );
    }

	@Test
	public void test() {
		User user = User.readUser("User_update_newKeyIsNull@teste.com");
		user.update(
				"newUser_update_newKeyIsNull@teste.com", 
        		"newUser_update_newKeyIsNull", 
        		null
        );
		Assert.assertEquals("newUser_update_newKeyIsNull@teste.com", user.getEmail());
		Assert.assertEquals("newUser_update_newKeyIsNull", user.getName());
		Assert.assertEquals(
				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhJexIekccp89SoWOn092roUid_-oYE33VWgYbmdMZMIVubwSp9GnU6woPYew22hlUEw5sGpQjvwYelp6-tziOzLLnz96v3HMp9fK1oGhsmxzMecmiVQ52XksbSztCLVhm5zFB0PQbKgvPtKJwDOywmGgfFE5UY5fldtI0BSJXYb7K5t6R5bMaRblwoDyz4U3S4-M75HkA4Ocz_zyznsvtVFh0jqqwu2Kc3DzO05y_MNYyG_XOii8RTCoJrZL1blfmvfwo_S1AvEZBlNXlw4RpcsVRK8Ci8RvgFJHAAnjTHK3tRiV_JkwVqlZASIgVx7IkJUL4KpKQ1HNu71aTB55oQIDAQAB", 
				Base64.getUrlEncoder().encodeToString(user.getPublicKey())
		);
		Assert.assertEquals(new ArrayList<UniqueDigit>(), user.getList());
	}
	
	@After
    public void clean() {
    	User.deleteUser("newUser_update_newKeyIsNull@teste.com");
    }

}
