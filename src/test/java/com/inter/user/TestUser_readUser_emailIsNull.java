package com.inter.user;

import org.junit.Assert;
import org.junit.Test;

public class TestUser_readUser_emailIsNull {
	
	@Test
	public void test() {
		Assert.assertEquals(null, User.readUser(null));
	}

}
