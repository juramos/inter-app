package com.inter.user;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestUser_createUser_duplicateEmail {

	@Before
    public void setUp() {
		User.createUser(
        		"User_createUser_duplicateEmail@teste.com", 
        		"User_createUser_duplicateEmail", 
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhuUbEUo2duP3T3rbcm291IkaZ9Kfj3biMW4ik--IyNA5aDp1YiR1xot6_dgE6S0R_rPNFfu0svCCc3EUin3S9lxfcNn3uQRam6uK7H11mB5BocJ4NZMRwi1BMcmB4NypqB0kV21l9P9VG5u3Gy_v8mGqYAFDnmiXdZj_FHQcD_H2ZqSDiy48PIaJ6-rjzHshgIm5Y06oHMfFsExb-5PmXRQFpk2RZw_1tZptKgt5Rwi0ry2Z-9FLwCSmQxovwVd2xSQOuYKQn9Dvy1jvV8y-JUyMbvjfHztut61UY9qQOV1MPMvwa__dzaZ0dHhwOlkJhkv0zQJpXBg9nyEjOAUjKQIDAQAB"
        );
    }
	
    @Test
    public void test() {
    	Assert.assertEquals(null, User.createUser(
        		"User_createUser_duplicateEmail@teste.com", 
        		"User_createUser_duplicateEmail", 
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhuUbEUo2duP3T3rbcm291IkaZ9Kfj3biMW4ik--IyNA5aDp1YiR1xot6_dgE6S0R_rPNFfu0svCCc3EUin3S9lxfcNn3uQRam6uK7H11mB5BocJ4NZMRwi1BMcmB4NypqB0kV21l9P9VG5u3Gy_v8mGqYAFDnmiXdZj_FHQcD_H2ZqSDiy48PIaJ6-rjzHshgIm5Y06oHMfFsExb-5PmXRQFpk2RZw_1tZptKgt5Rwi0ry2Z-9FLwCSmQxovwVd2xSQOuYKQn9Dvy1jvV8y-JUyMbvjfHztut61UY9qQOV1MPMvwa__dzaZ0dHhwOlkJhkv0zQJpXBg9nyEjOAUjKQIDAQAB"
        ));
    }
    
    @After
    public void clean() {
    	User.deleteUser("User_createUser_duplicateEmail@teste.com");
    }
}
