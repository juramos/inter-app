package com.inter.user;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.inter.digit.UniqueDigit;

public class TestUser_readUser_checkList {

	@Before
	public void setUp() throws Exception {
		String email = "User_readUser_checkList@teste.com";
		User.createUser(email, "User_readUser_checkList", null);
		UniqueDigit.createUniqueDigit("9875", 1, email);
		UniqueDigit.createUniqueDigit("9875", 4, email);
		UniqueDigit.createUniqueDigit("116", 1, email);
	}

	@Test
	public void test() {
		User user = User.readUser("User_readUser_checkList@teste.com");
		
		List<UniqueDigit> list = user.getList();
		Assert.assertEquals(3, list.size());
		
		UniqueDigit digit = list.get(0);
		Assert.assertEquals("9875", digit.getN());
		Assert.assertEquals(1, digit.getK());
		Assert.assertEquals(2, digit.getResult());
		
		digit = list.get(1);
		Assert.assertEquals("9875", digit.getN());
		Assert.assertEquals(4, digit.getK());
		Assert.assertEquals(8, digit.getResult());
		
		digit = list.get(2);
		Assert.assertEquals("116", digit.getN());
		Assert.assertEquals(1, digit.getK());
		Assert.assertEquals(8, digit.getResult());		
	}
    
    @After
    public void clean() {
    	User.deleteUser("User_readUser_checkList@teste.com");
    }


}
