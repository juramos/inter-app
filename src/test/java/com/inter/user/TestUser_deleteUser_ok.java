package com.inter.user;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestUser_deleteUser_ok {

	@Before
	public void setUp() {
		User.createUser(
        		"User_deleteUser_ok@teste.com", 
        		"User_deleteUser_ok", 
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkPnFas3UizBVM9c-9rgtvM5dY7CqWZIjpDna8anrayWwjRCVRHBzzMznjetXO4BNcf1tPMUqjduBUr7lB2ko1LbmmyAexf2tviGfgFyFTeoGNVN1hrvCihXNTSUgdWhvnKA-_74Pv2tTx5vmApK-qTv-DwCB3APdlSqU7eXX3FSzrgpBgkaXOYhPsdfuRhQdmH9FeOIpMwxBXoYAqgFrguptPbtp0fTvejhk5AqZjhGc28M3kFH2xnzJYDY6wSpcOo1YXuFokkEjbKXNZJXJrO9fnttjXTcV_CP7aStL_v6uNQ56ESw9MOtx8yds-00p0MYdVyLUw4NWdDimtx-L1wIDAQAB"
        );
	}

	@Test
	public void test() {
		Assert.assertEquals(true, User.deleteUser("User_deleteUser_ok@teste.com"));
	}

}
