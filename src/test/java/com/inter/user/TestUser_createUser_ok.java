package com.inter.user;

import java.util.ArrayList;
import java.util.Base64;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import com.inter.digit.UniqueDigit;

public class TestUser_createUser_ok {
	
    @Test
    public void test() {
    	String email = "User_createUser_ok@teste.com";
    	String name = "User_createUser_ok";
    	String key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhJexIekccp89SoWOn092roUid_-oYE33VWgYbmdMZMIVubwSp9GnU6woPYew22hlUEw5sGpQjvwYelp6-tziOzLLnz96v3HMp9fK1oGhsmxzMecmiVQ52XksbSztCLVhm5zFB0PQbKgvPtKJwDOywmGgfFE5UY5fldtI0BSJXYb7K5t6R5bMaRblwoDyz4U3S4-M75HkA4Ocz_zyznsvtVFh0jqqwu2Kc3DzO05y_MNYyG_XOii8RTCoJrZL1blfmvfwo_S1AvEZBlNXlw4RpcsVRK8Ci8RvgFJHAAnjTHK3tRiV_JkwVqlZASIgVx7IkJUL4KpKQ1HNu71aTB55oQIDAQAB"; 
    	User user = User.createUser(email, name, key);
        Assert.assertEquals(user.getEmail(), email);
        Assert.assertEquals(user.getName(), name);
        Assert.assertEquals(Base64.getUrlEncoder().encodeToString(user.getPublicKey()), key);
        Assert.assertEquals(user.getList(), new ArrayList<UniqueDigit>());
    }
    
    @After
    public void clean() {
    	User.deleteUser("User_createUser_ok@teste.com");
    }

}
