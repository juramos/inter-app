package com.inter.user;

import java.util.ArrayList;
import java.util.Base64;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.inter.digit.UniqueDigit;

public class TestUser_update_newEmailIsNull {

	@Before
    public void setUp() {
		User.createUser(
        		"User_update_newEmailIsNull@teste.com", 
        		"User_update_newEmailIsNull",
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhJexIekccp89SoWOn092roUid_-oYE33VWgYbmdMZMIVubwSp9GnU6woPYew22hlUEw5sGpQjvwYelp6-tziOzLLnz96v3HMp9fK1oGhsmxzMecmiVQ52XksbSztCLVhm5zFB0PQbKgvPtKJwDOywmGgfFE5UY5fldtI0BSJXYb7K5t6R5bMaRblwoDyz4U3S4-M75HkA4Ocz_zyznsvtVFh0jqqwu2Kc3DzO05y_MNYyG_XOii8RTCoJrZL1blfmvfwo_S1AvEZBlNXlw4RpcsVRK8Ci8RvgFJHAAnjTHK3tRiV_JkwVqlZASIgVx7IkJUL4KpKQ1HNu71aTB55oQIDAQAB"
        );
    }

	@Test
	public void test() {
		User user = User.readUser("User_update_newEmailIsNull@teste.com");
		user.update(
				null, 
        		"newUser_update_newEmailIsNull", 
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApbGPiMib35mhPC0oEeUVHuCWm37vpdWXLKsSZrTPaA5PsGKEnA1_z5_5OL3nreKzKXP_RmI3-XpkXbejPLoGt0nyXRgw1B5qMJSboYAjX8gC-4VBZ34UGMskeFvy7M8TvE-uTdddHCOaJM4IDEJu34KT5f5IcNmIZX0GEsK2uKXWBgnCqm-q4dW6_n0YI7MVFSmhtuDnJHE81JLO-eTGs0_19minXuYS3a31Py64KmSr4Dj7Ozd4SV9uznk7kckLt2i7TVz0xj82ZIc5ukCSM10En43eidhBxuTtg1cu47O_63OWDy57dyfZGIHXtVIY67wMX5dihCfbNHugSMkOjQIDAQAB"
        );
		Assert.assertEquals("User_update_newEmailIsNull@teste.com", user.getEmail());
		Assert.assertEquals("newUser_update_newEmailIsNull", user.getName());
		Assert.assertEquals(
				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApbGPiMib35mhPC0oEeUVHuCWm37vpdWXLKsSZrTPaA5PsGKEnA1_z5_5OL3nreKzKXP_RmI3-XpkXbejPLoGt0nyXRgw1B5qMJSboYAjX8gC-4VBZ34UGMskeFvy7M8TvE-uTdddHCOaJM4IDEJu34KT5f5IcNmIZX0GEsK2uKXWBgnCqm-q4dW6_n0YI7MVFSmhtuDnJHE81JLO-eTGs0_19minXuYS3a31Py64KmSr4Dj7Ozd4SV9uznk7kckLt2i7TVz0xj82ZIc5ukCSM10En43eidhBxuTtg1cu47O_63OWDy57dyfZGIHXtVIY67wMX5dihCfbNHugSMkOjQIDAQAB", 
				Base64.getUrlEncoder().encodeToString(user.getPublicKey())
		);
		Assert.assertEquals(new ArrayList<UniqueDigit>(), user.getList());
	}
	
	@After
    public void clean() {
    	User.deleteUser("User_update_newEmailIsNull@teste.com");
    }

}
