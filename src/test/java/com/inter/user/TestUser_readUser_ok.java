package com.inter.user;

import java.util.ArrayList;
import java.util.Base64;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.inter.digit.UniqueDigit;

public class TestUser_readUser_ok {

	@Before
	public void setUp() {
		User.createUser(
        		"User_readUser_ok@teste.com", 
        		"User_readUser_ok", 
        		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkPnFas3UizBVM9c-9rgtvM5dY7CqWZIjpDna8anrayWwjRCVRHBzzMznjetXO4BNcf1tPMUqjduBUr7lB2ko1LbmmyAexf2tviGfgFyFTeoGNVN1hrvCihXNTSUgdWhvnKA-_74Pv2tTx5vmApK-qTv-DwCB3APdlSqU7eXX3FSzrgpBgkaXOYhPsdfuRhQdmH9FeOIpMwxBXoYAqgFrguptPbtp0fTvejhk5AqZjhGc28M3kFH2xnzJYDY6wSpcOo1YXuFokkEjbKXNZJXJrO9fnttjXTcV_CP7aStL_v6uNQ56ESw9MOtx8yds-00p0MYdVyLUw4NWdDimtx-L1wIDAQAB"
        );
	}

	@Test
	public void test() {
		User user = User.readUser("User_readUser_ok@teste.com");
		Assert.assertEquals("User_readUser_ok@teste.com", user.getEmail());
		Assert.assertEquals("User_readUser_ok", user.getName());
		Assert.assertEquals(
				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkPnFas3UizBVM9c-9rgtvM5dY7CqWZIjpDna8anrayWwjRCVRHBzzMznjetXO4BNcf1tPMUqjduBUr7lB2ko1LbmmyAexf2tviGfgFyFTeoGNVN1hrvCihXNTSUgdWhvnKA-_74Pv2tTx5vmApK-qTv-DwCB3APdlSqU7eXX3FSzrgpBgkaXOYhPsdfuRhQdmH9FeOIpMwxBXoYAqgFrguptPbtp0fTvejhk5AqZjhGc28M3kFH2xnzJYDY6wSpcOo1YXuFokkEjbKXNZJXJrO9fnttjXTcV_CP7aStL_v6uNQ56ESw9MOtx8yds-00p0MYdVyLUw4NWdDimtx-L1wIDAQAB", 
				Base64.getUrlEncoder().encodeToString(user.getPublicKey())
		);
		Assert.assertEquals(new ArrayList<UniqueDigit>(), user.getList());
	}
    
    @After
    public void clean() {
    	User.deleteUser("User_readUser_ok@teste.com");
    }

}
